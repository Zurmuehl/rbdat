rm *.o *.so
gfortran -fno-optimize-sibling-calls  -fpic  -g -O2 -mtune=native -Wall -pedantic -flto=10 -c BDATpro.f -o BDATpro.o && \
    gfortran -fno-optimize-sibling-calls  -fpic  -g -O2 -mtune=native -Wall -pedantic -flto=10 -c FormTarife.f -o FormTarife.o && \
    gfortran -fno-optimize-sibling-calls  -fpic  -g -O2 -mtune=native -Wall -pedantic -flto=10 -c Koeff.f -o Koeff.o && \
    gcc -I"/home/qwer/svn/R/r-devel/build/include" -DNDEBUG   -I/usr/local/include   -fpic  -g -O2 -Wall -pedantic -mtune=native -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong -fstack-clash-protection -fcf-protection -Werror=implicit-function-declaration -flto=10 -c rBDAT_init.c -o rBDAT_init.o && \
    gfortran -fno-optimize-sibling-calls  -fpic  -g -O2 -mtune=native -Wall -pedantic -flto=10 -c vBDATpro.f -o vBDATpro.o && \
    gcc -shared -g -O2 -Wall -pedantic -mtune=native -Werror=format-security \
    -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong \
    -fstack-clash-protection -fcf-protection \
    -Werror=implicit-function-declaration -flto=10 -fpic -o rBDAT.so BDATpro.o \
    FormTarife.o Koeff.o rBDAT_init.o vBDATpro.o -lgfortran -lm -lquadmath > cranchecks.out 2>&1
